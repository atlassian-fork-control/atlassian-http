package com.atlassian.http.mime;

import com.google.common.collect.ImmutableMap;
import java.util.HashMap;

/**
 * This class can be used to retrieve what HTTP headers and values should be sent on a given download.
 */

public class DownloadHeaderHelper {
    private String contentDisposition;
    private String contentType;

    private final static  ImmutableMap<String, String> CONSTANT_HEADER =
        new ImmutableMap.Builder<String, String>()
            .put("X-Content-Type-Options", "nosniff")
            .put("X-Download-Options", "noopen")
            .build();

    /**
     *
     * @param contentDispositionHeaderGuesser an appropriately configured ContentDispositionHeaderGuesser with which
     *                                        to determine the download content disposition type.
     * @param fileName the name of the file
     * @param contentType the download content-type
     * @param userAgent the browser user agent.
     */

    public DownloadHeaderHelper(ContentDispositionHeaderGuesser contentDispositionHeaderGuesser, String fileName, String contentType, String userAgent)
    {
        this.contentType = contentType;
        this.contentDisposition = contentDispositionHeaderGuesser.guessContentDispositionHeader(fileName, this.contentType, userAgent);
    }

    /**
     *
     * @param contentDisposition inline or attachment
     * @param fileName the name of the file
     * @param contentType the download content-type
     */
    public DownloadHeaderHelper(String contentDisposition, String fileName, String contentType) {
        this.contentDisposition = contentDisposition;
        this.contentType = contentType;
    }

    /**
     *
     * @param contentDisposition inline or attachment
     * @param contentType the download content-type
     */
    public DownloadHeaderHelper(String contentDisposition, String contentType) {
        this(contentDisposition, null, contentType);
    }

    /**
     *
     * @return a HashMap of HTTP headers and values
     */
    public HashMap<String, String> getDownloadHeaders() {
        HashMap<String, String> headers = new HashMap<String, String>();
        headers.putAll(CONSTANT_HEADER);
        headers.put("Content-Type", this.contentType);
        headers.put("Content-Disposition", this.contentDisposition);
        return headers;
    }

    public String getContentDisposition() {
        return contentDisposition;
    }

    public String getContentType() {
        return contentType;
    }


}
