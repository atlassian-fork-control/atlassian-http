package com.atlassian.http.url;

import com.google.common.annotations.VisibleForTesting;

import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.Locale;

/**
 * This class can be used to check if two URLs are in the same origin.
 *
 * @since 1.0.10.
 */
public class SameOrigin
{
    /**
     * @param url the url to check
     * @param origin the origin to check against.
     * @return true if the given url is the same origin as the given origin url
     * otherwise returns false.
     */
    public static boolean isSameOrigin(URL url, URL origin)
    {
        if (url == null || origin == null)
        {
            return false;
        }
        try
        {
            url.toURI();
            origin.toURI();
        }
        catch (URISyntaxException e)
        {
            return false;
        }
        final String originHost = origin.getHost().toLowerCase(Locale.US);
        final String urlHost = url.getHost().toLowerCase(Locale.US);
        return origin.getProtocol().equals(url.getProtocol()) &&
            getPortForUrl(origin) == getPortForUrl(url) &&
            originHost.equals(urlHost);
    }

    /**
     * @param uri the uri to check
     * @param origin the origin to check against.
     * @return true if the given uri is the same origin as the given origin uri
     * otherwise returns false.
     * @throws MalformedURLException
     * @since 1.1.0.
     */
    public static boolean isSameOrigin(URI uri, URI origin) throws MalformedURLException
    {
        if (uri == null || origin == null)
        {
            return false;
        }
        return isSameOrigin(uri.toURL(), origin.toURL());
    }

    /**
     * Returns the port for a given url.
     * @param url the url to get the port of.
     * @return the port for a given url.
     */
    @VisibleForTesting
    static int getPortForUrl(URL url)
    {
        if (url.getPort() != -1)
        {
            return url.getPort();
        }
        return url.getDefaultPort();
    }
}
