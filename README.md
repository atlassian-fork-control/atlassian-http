## atlassian-http ##
Welcome to the atlassian-http project.

Jira Monolith has forked this repo. See ticket CPLATFORM-2104 for more details"

### Releases ###
* Please use **2.x.x** releases and do **not** use the old **0.0.x.x** release versions.

### Tests ###
To run the tests simply execute

    mvn clean test

### Builds ###
Builds are found at [https://ecosystem-bamboo.internal.atlassian.com/browse/HTTP](https://ecosystem-bamboo.internal.atlassian.com/browse/HTTP).
